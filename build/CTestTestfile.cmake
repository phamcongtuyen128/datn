# CMake generated Testfile for 
# Source directory: /workspace/src
# Build directory: /workspace/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("rabbitamr_description")
subdirs("beginner_tutorials")
subdirs("rrt")
subdirs("student_scan_matcher")
subdirs("f1tenth_simulator")
subdirs("AMR_Description")
subdirs("model_1")
subdirs("model_2")
subdirs("agv_tb_description")
subdirs("agv_tb_gazebo")
subdirs("mastering_ros_robot_description_pkg")
