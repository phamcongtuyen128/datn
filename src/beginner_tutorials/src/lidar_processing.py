#!/usr/bin/env python

import queue
import rospy
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float64
from beginner_tutorials.msg import scan_range

class Lab1Class():
    def __init__(self):   #define Lab1Class constructure
        rospy.init_node("lab1_python")
        
        #create a Subscriber to /scan topic, use messages of LaserScan and call laser_callback
        self.scan_sub_ = rospy.Subscriber('/scan', LaserScan, self.laser_callback) 
        self.closet_pub_ = rospy.Publisher('/closet_scan', Float64, queue_size=10) 
        self.farthest_pub_= rospy.Publisher('/farthest_scan',Float64, queue_size=10)

    def laser_callback(self, msgs):
        # msgs = LaserScan()
        closet_data = min(msgs.ranges)
        closet = Float64()
        closet.data = closet_data
        self.closet_pub_.publish(closet)
        farthest_data = max(msgs.ranges)
        farthest = Float64()
        farthest.data = farthest_data
        self.farthest_pub_.publish(farthest)
        # max_range = max(msgs.ranges)

lab1 = Lab1Class()
rospy.spin()