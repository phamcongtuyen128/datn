;; Auto-generated. Do not edit!


(when (boundp 'beginner_tutorials::scan_range)
  (if (not (find-package "BEGINNER_TUTORIALS"))
    (make-package "BEGINNER_TUTORIALS"))
  (shadow 'scan_range (find-package "BEGINNER_TUTORIALS")))
(unless (find-package "BEGINNER_TUTORIALS::SCAN_RANGE")
  (make-package "BEGINNER_TUTORIALS::SCAN_RANGE"))

(in-package "ROS")
;;//! \htmlinclude scan_range.msg.html


(defclass beginner_tutorials::scan_range
  :super ros::object
  :slots (_max _min ))

(defmethod beginner_tutorials::scan_range
  (:init
   (&key
    ((:max __max) 0.0)
    ((:min __min) 0.0)
    )
   (send-super :init)
   (setq _max (float __max))
   (setq _min (float __min))
   self)
  (:max
   (&optional __max)
   (if __max (setq _max __max)) _max)
  (:min
   (&optional __min)
   (if __min (setq _min __min)) _min)
  (:serialization-length
   ()
   (+
    ;; float64 _max
    8
    ;; float64 _min
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _max
       (sys::poke _max (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _min
       (sys::poke _min (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _max
     (setq _max (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _min
     (setq _min (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get beginner_tutorials::scan_range :md5sum-) "1be1f057ba1e6e938ede7aada413094e")
(setf (get beginner_tutorials::scan_range :datatype-) "beginner_tutorials/scan_range")
(setf (get beginner_tutorials::scan_range :definition-)
      "float64 max
float64 min

")



(provide :beginner_tutorials/scan_range "1be1f057ba1e6e938ede7aada413094e")


